package com.divergentsl.cms.login;


import java.util.InputMismatchException;
import java.util.Scanner;
import com.divergentsl.cms.admin.AdminHomePage;
import com.divergentsl.cms.doctor.DoctorHomePage;
import com.divergentsl.cms.patients.PatientHomePage;
import com.divergentsl.cms.utils.PageFormate;

public class Loign {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(PageFormate.pageSeprater);
		System.out.println("\n---------------------------- Welcome to Clinc Mangement System ------------------------------");
		Loign.showLoginMenu();
	}

	public static void showLoginMenu() {
		// TODO Auto-generated method stubs
		
		
		while (true) {
			
		try {
		System.out.println(PageFormate.pageSeprater);
		System.out.println("\n------------------------ CMS Home Page --------------------------- \n");
		System.out.println("------> Choose the mode of login. \n");	
			
		System.out.println("Enter 1 for login as Admin :");
		System.out.println("Enter 2 for login as Doctor :");
		System.out.println("Enter 3 for login as Patient :");
		System.out.println("Enter 4 for Exiting the System :");
		
		Scanner scan =  new Scanner(System.in);
		int choice = scan.nextInt();
		

		switch(choice){
			
		case (1):
			AdminHomePage.welcomeAdmin();
			break;
			
		case (2):
			DoctorHomePage.authenticateDoctor();
			break;
		
		case (3):
			PatientHomePage.authenticatePatient();
			break;
		
		case (4):
			System.out.println(PageFormate.exitGreet);
			System.exit(1);
			
		default:
			System.out.println(PageFormate.wrongChoice);
			break;
		}
	
		}catch(InputMismatchException e) {
			System.out.println(PageFormate.invalidInput);
		}
	}	

	}

}
