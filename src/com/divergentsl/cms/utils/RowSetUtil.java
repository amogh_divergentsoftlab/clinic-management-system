package com.divergentsl.cms.utils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetProvider;

public class RowSetUtil {
	
	public static JdbcRowSet rowSetProvider() {
		
		Properties prop = new Properties();
		
		try(InputStream ins = Files.newInputStream(Paths.get("./resources/database.properties"));
			JdbcRowSet rowset = RowSetProvider.newFactory().createJdbcRowSet()){
			prop.load(ins);
			
			rowset.setUrl(prop.getProperty("jdbc.url"));
			rowset.setUsername(prop.getProperty("jdbc.user"));
			rowset.setPassword(prop.getProperty("jdbc.password"));
			
			return rowset;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return null;
	}

}
