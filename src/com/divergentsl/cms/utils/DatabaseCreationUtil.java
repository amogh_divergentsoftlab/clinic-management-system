package com.divergentsl.cms.utils;


import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;

public class DatabaseCreationUtil {

	private static String[] createTables  = {"create table Doctor(Doctor_ID int not null primary key auto_increment,Name varchar(50), Age int(2), Contact_No bigint, Specialization varchar(50), Degree varchar(50), Fee decimal(7,2));",
											"create table Patient(Patient_ID int not null primary Key auto_increment, Name varchar(50), Age int(2), Gender varchar(6), Contact_No bigint, Disease varchar(30), Status varchar(40));", 
											"create table Drugs(Drug_ID int not null primary Key auto_increment, Name varchar(20), ManufacturingDate varchar(7), ExpiryDate varchar(7), Charges decimal(9,2));",
											"create table LabTest(LabTest_ID int not null auto_increment primary key, Patient_ID int not null, Doctor_ID int not null, LabTestFor varchar(20), Charges int(5), Foreign key(Patient_ID) references Patient(Patient_ID), Foreign key(Doctor_ID) references Doctor(Doctor_ID));",
											"create table Appointment(Appointment_ID int not null primary key auto_increment, Doctor_ID int not null, Patient_ID int not null, Time varchar(5), Date varchar(10), Foreign key(Doctor_ID) references doctor(Doctor_ID), Foreign key(Patient_ID) references patient(Patient_ID));",
											"create table prescription(Prescription_ID int not null primary key auto_increment, Doctor_ID int not null, Patient_ID int not null, Drugs_ID int not null, Labtest int not null, Prescription varchar(120));",
											"create table invoice(Invoice_ID int not null primary key auto_increment, Patient_ID int not null, Doctor_Charges decimal(11,2), labtest_Charges decimal(11,2), Drugs_Charges decimal(11,2), Total_Amount decimal(11,2), Foreign key(Patient_ID) references patient(Patient_ID));"};
	
	private static String[] hashingAndAlteringID = {"alter table Doctor auto_increment = 10000;", 
										"alter table Patient auto_increment = 1000000;",
										"alter table Drugs auto_increment = 1000000;",
										"alter table labTest auto_increment = 1000000;",
										"alter table Appointment auto_increment = 10000;",
										"alter table prescription add Foreign key(Doctor_ID) references doctor(Doctor_ID);",
										"alter table prescription add Foreign key(Patient_ID) references patient(Patient_ID);",
										"alter table prescription add Foreign key(Drugs_ID) references drugs(Drug_ID);",
										"alter table prescription add Foreign key(Labtest) references labtest(LabTest_ID);", 
										"alter table prescription auto_increment = 1001;",
										"alter table invoice auto_increment = 1230;"};
	
	public static void main(String[] args) throws SQLException, IOException {
		// TODO Auto-generated method stub

		try (java.sql.Connection connection = ConnectionUtil.createConncetion()){
		
		for (int i = 0; i < createTables.length; i++) {
			Statement statement = connection.createStatement();
			statement.execute(createTables[i]);
		}
		
		for (int i = 0; i < hashingAndAlteringID.length; i++) {
			Statement statement = connection.createStatement();
			statement.execute(hashingAndAlteringID[i]);
		}
		
		}
		
	}

}
