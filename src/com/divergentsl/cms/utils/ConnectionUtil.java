package com.divergentsl.cms.utils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionUtil {

	public static java.sql.Connection createConncetion() throws SQLException, IOException {
		
		Properties prop = new Properties();
		
		try(InputStream ins = Files.newInputStream(Paths.get("./resources/database.properties"))){
			prop.load(ins);
		}
		
		//String driver = prop.getProperty("jdbc.Driver");
		String url = prop.getProperty("jdbc.url");
		String user = prop.getProperty("jdbc.user");
		String password = prop.getProperty("jdbc.password");
		
		return DriverManager.getConnection(url, user, password);
		
	}
}
