package com.divergentsl.cms.dto;

import com.divergentsl.cms.dao.PatientOperation;

public class Patient {

	private int id;
	
	private String name;
	
	private int age;
	
	private String gender;

	private long contactNo;
	
	private String disease;
	
	private String status;

	public Patient(int id, String name, int age, String gender, long contactNo, String disease, String status) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.contactNo = contactNo;
		this.disease = disease;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public long getContactNo() {
		return contactNo;
	}

	public String getDisease() {
		return disease;
	}

	public String getGender() {
		return gender;
	}
	
	public String getStatus() {
		return status;
	}
	
	
	public static void getPatientInfo(int patientId) {
		
	Patient patient = PatientOperation.viewPatientDetails(patientId);
	
	if (patient == null)
		return;
	System.out.println("Here are the patient details : \n");

	System.out.println("Patient ID          : "  + patient.getId());
	System.out.println("Patient name        : "  + patient.getName());
	System.out.println("Patient age         : "  + patient.getAge());
	System.out.println("Patient gender      : "  + patient.getGender());
	System.out.println("Patient contact No. : "  + patient.getContactNo());
	System.out.println("Patient diseases    : "  + patient.getDisease());
	System.out.println("Patient status      : "  + patient.getStatus());
		
	}
}
