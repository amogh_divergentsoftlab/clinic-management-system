package com.divergentsl.cms.dto;

import java.util.List;

import com.divergentsl.cms.dao.InvoiceOperations;

public class Invocie {

	private int id;
	
	private int patientId;
	
	private double doctorCharges;
	
	private double labtestCharges;
	
	private double drugsCharges;
	
	private double totalAmount;

	public Invocie(int id, int patientId, double doctorCharges, double labtestCharges, double drugsCharges,
			double totalAmount) {
		super();
		this.id = id;
		this.patientId = patientId;
		this.doctorCharges = doctorCharges;
		this.labtestCharges = labtestCharges;
		this.drugsCharges = drugsCharges;
		this.totalAmount = totalAmount;
	}

	public int getId() {
		return id;
	}

	public int getPatientId() {
		return patientId;
	}

	public double getDoctorCharges() {
		return doctorCharges;
	}

	public double getLabtestCharges() {
		return labtestCharges;
	}

	public double getDrugsCharges() {
		return drugsCharges;
	}

	public double getTotalAmount() {
		return totalAmount;
	}
	
	public static void getInvoiceInfo(int patientId) {
		
		List<Invocie> invoices = InvoiceOperations.viewInvoice(patientId);
		
		if(invoices == null)
			return;
		
		System.out.println("------> Here are the Invoices of Patient ID : " + patientId + "  from latest to earliest \n");
		int count = 1;
		
		for (Invocie invoice : invoices) {
			
			System.out.println("Invoice : " + count +  "\n");
			
			System.out.println("Invoice Id        :- " + invoice.getId() );	
			System.out.println("Patient Id        :- " + invoice.getPatientId() );	
			System.out.println("Doctor Charges    :- " + invoice.getDoctorCharges() );	
			System.out.println("Labtest Charges   :- " + invoice.getLabtestCharges() );	
			System.out.println("Drugs Charges     :- " + invoice.getDrugsCharges() );	
			System.out.println("Total Charges     :- " + invoice.getTotalAmount() );	

			count++;
		}
			
	}
}
