package com.divergentsl.cms.dto;

import java.util.List;

import com.divergentsl.cms.dao.PatientOperation;
import com.divergentsl.cms.dao.PrescriptionOperation;

public class Prescription {

	private int id;
	
	private int doctorId;
	
	private int patientId;
	
	private int labtestId;
	
	private int drugId;
	
	private String prescription;

	public Prescription(int id, int doctorId, int patientId, int drugId,  int labtestId, String prescription) {
		super();
		this.id = id;
		this.doctorId = doctorId;
		this.patientId = patientId;
		this.labtestId = labtestId;
		this.drugId = drugId;
		this.prescription = prescription;
	}

	public int getId() {
		return id;
	}

	public int getDoctorId() {
		return doctorId;
	}

	public int getPatientId() {
		return patientId;
	}

	public int getLabtestId() {
		return labtestId;
	}

	public int getDrugId() {
		return drugId;
	}

	public String getPrescription() {
		return prescription;
	}
	
	
	public static void getPrescritionInfo(int patientId) {
	
		System.out.println("\n--------------------------- PRESCRIPTION PAGE ----------------------------\n");
		
		if(!PatientOperation.chcekExistance(patientId)) {
			return;
		}
		
		List<Prescription> prescription =  PrescriptionOperation.viewPrescription(patientId);
		
		if (prescription == null) {
			System.out.println("------> No prescripition for patient ID : " + patientId);
			return;
		}
	
		int count = 1;
		for (Prescription pres : prescription) {
			
			System.out.println("\n-----------------------------");
			System.out.println("Prescription " + count + " : \n");
			
			System.out.println("Prescription ID : " + pres.getId() );
		
			System.out.println("\nPatient Info : ------" );
			Patient.getPatientInfo(patientId);
			
			System.out.println("\nLabtest Info : ------");
			Labtest.viewLabtestInfo(pres.getLabtestId());
			
			System.out.println("\nDrugs Info : ------");
			Drugs.getDrugsInfo(pres.getDrugId());
			
			System.out.println("\nPrescription : ------");
			System.out.println(pres.getPrescription());
			
			count++;
		}
	}
}
