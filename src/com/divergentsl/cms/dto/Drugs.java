package com.divergentsl.cms.dto;

import com.divergentsl.cms.dao.DrugsOperation;

public class Drugs {

	private int drugId;
	
	private String name;

	private String manufacturingDate;
	
	private String expiryDate;
		
	private double charges;

	public Drugs(int drugId, String name, String manufacturingDate, String expiryDate, double charges) {
		super();
		this.drugId = drugId;
		this.manufacturingDate = manufacturingDate;
		this.expiryDate = expiryDate;
		this.name = name;
		this.charges = charges;
	}

	public int getDrugId() {
		return drugId;
	}

	public String getManufacturingDate() {
		return manufacturingDate;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public String getName() {
		return name;
	}

	public double getCharges() {
		return charges;
	}
	
	
	public static void getDrugsInfo(int drugsId) {
		
		Drugs drugs = DrugsOperation.viewDrugs(drugsId);
		
		if (drugs == null)
			return;
		
		System.out.println("Drug ID                 : " + drugs.getDrugId());
		System.out.println("Drug Name               : " + drugs.getName());
		System.out.println("Drug manufacturing date : " + drugs.getManufacturingDate());
		System.out.println("Drug expiry date        : " + drugs.getExpiryDate());
		System.out.println("Drrug Charges           : " + drugs.getCharges());
	
	}
	
}
