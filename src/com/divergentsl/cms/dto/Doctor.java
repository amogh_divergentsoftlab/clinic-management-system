package com.divergentsl.cms.dto;

import com.divergentsl.cms.dao.DoctorOperations;

public class Doctor {

	private int id;
	
	private String name;
	
	private int age;
	
	private long contactNo;
	
	private String specialization;
		
	private String degree;
	
	private double fee;

	public Doctor(int id, String name, int age, long contactNo, String specialization, String degree, double fee) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.contactNo = contactNo;
		this.specialization = specialization;
		this.degree = degree;
		this.fee = fee;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public String getSpecialization() {
		return specialization;
	}

	public double getFee() {
		return fee;
	}

	public String getDegree() {
		return degree;
	}

	public long getContactNo() {
		return contactNo;
	}

	public int getAge() {
		return age;
	}
	
	public static void viewDoctorInfo(int doctorId) {
		
	Doctor doctor = DoctorOperations.viewDoctorDetails(doctorId);
	
	if(doctor == null)
		return;
		
	System.out.println("Doctor ID              : " + doctor.getId());
	System.out.println("Doctor Name            : " + doctor.getName());
	System.out.println("Doctor Age 	           : " + doctor.getAge());
	System.out.println("Doctor Contact No.     : " + doctor.getContactNo());
	System.out.println("Doctor Specialization  : " + doctor.getSpecialization());
	System.out.println("Doctor Degree          : " + doctor.getDegree());
	System.out.println("Doctor Fee	           : " + doctor.getFee());
	
	}
}
