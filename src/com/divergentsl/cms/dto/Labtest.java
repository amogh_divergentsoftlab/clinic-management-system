package com.divergentsl.cms.dto;

import com.divergentsl.cms.dao.LabtestOperations;

public class Labtest {

	private int id;
	
	private int patientId;
	
	private int doctorId;
	
	private String labtestFor;
	
	private double charges;

	public Labtest(int id, int patientId, int doctorId, String labtestFor, double charges) {
		super();
		this.id = id;
		this.patientId = patientId;
		this.doctorId = doctorId;
		this.labtestFor = labtestFor;
		this.charges = charges;
	}
	
	
	
	public int getId() {
		return id;
	}

	public int getPatientId() {
		return patientId;
	}

	public int getDoctorId() {
		return doctorId;
	}

	public String getLabtestFor() {
		return labtestFor;
	}

	public double getCharges() {
		return charges;
	}

	public static void viewLabtestInfo(int labtestId) {
		
		Labtest labtest = LabtestOperations.viewLabtest(labtestId);
		
		if(labtest == null)
			return;

		System.out.println("Labtest ID          : " + labtest.getId());
		System.out.println("Labtest Patient ID  : " + labtest.getPatientId());
		System.out.println("Labtest Doctor ID   : " + labtest.getDoctorId());
		System.out.println("Labtest Labtest for : " + labtest.getLabtestFor());
		System.out.println("Labtest Charges     : " + labtest.getCharges());

		
	}
}
