package com.divergentsl.cms.dto;

import java.util.List;
import java.util.Scanner;

import com.divergentsl.cms.dao.AppointmentOperation;
import com.divergentsl.cms.dao.DoctorOperations;
import com.divergentsl.cms.doctor.DoctorHomePage;
import com.divergentsl.cms.doctor.Prescription;
import com.divergentsl.cms.utils.PageFormate;

public class Appointment {

	public static void getAppointmentInfo(int doctorId) {
		
		if(!DoctorOperations.checkExistance(doctorId))
			return;
				
		List<String> apps = AppointmentOperation.getAppointment(doctorId);
		
		if (apps == null)
			return;
		
		if (apps.size() == 0) {
			System.out.println("\n-------------------------- No Appointment scheduled ----------------------------------");
			return;
		}
		
		while(true) {
		
		try{
	
		int count = 0;
		Scanner scan = new Scanner(System.in);
		
		System.out.println("\n------> Enter your choice : \n");
		System.out.println("Entet 0 for retruning to Doctor Home Page :");

		for(String patientId : apps) {
			count++;
			System.out.println("Enter "+ count + " to create prescription for patient at ID " + apps.get(count-1));
		}
		
		int choice = Integer.parseInt(scan.nextLine());
		
		if(choice == 0) {
			DoctorHomePage.welcomeDoctor(doctorId);
			return;
		}
		
		choice = choice - 1;
		Prescription.createPrescription(doctorId,Integer.parseInt(apps.get(choice)));
		
		}catch (NumberFormatException e) {
			// TODO: handle exception
			System.out.println(PageFormate.invalidInput);
		}
		catch(IndexOutOfBoundsException e) {
			System.out.println(PageFormate.wrongChoice);
		}
		
		}
		
		}
}
