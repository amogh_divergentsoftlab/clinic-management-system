package com.divergentsl.cms.doctor;

import java.util.List;
import java.util.Scanner;

import com.divergentsl.cms.dao.AppointmentOperation;
import com.divergentsl.cms.utils.PageFormate;

public class AppointmentViewer {
	
	
	public static void viewAppointments(int doctorId) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);

		System.out.println(PageFormate.pageSeprater);
		System.out.println("---------------------------- APPOINTMENT VIEW PAGE ----------------------------");

		while(true) {
			
		try {
				
		List<String> apps = AppointmentOperation.getAppointment(doctorId);
		
		if(apps == null) {
			return;
		}
		
		if (apps.size() == 0) {
			//System.out.println(PageFormate.pageSeprater);
			System.out.println("\n-------------------------- No Appointment scheduled ----------------------------------");
			return;
		}
		
		int count = 0;
		System.out.println("\n------> Enter your choice : \n");
		System.out.println("Entet 0 for retruning to Doctor Home Page :");

		for(String patientId : apps) {
			count++;
			System.out.println("Enter "+ count + " to create prescription for patient at ID " + apps.get(count-1));
		}
		
		int choice = Integer.parseInt(scan.nextLine());
		
		if(choice == 0) {
			DoctorHomePage.welcomeDoctor(doctorId);
			return;
		}
		
		choice = choice - 1;
		Prescription.createPrescription(doctorId,Integer.parseInt(apps.get(choice)));
		break;
		
		}catch(NumberFormatException e) {
			System.out.println(PageFormate.invalidInput + "\n");
		}catch(IndexOutOfBoundsException e) {
			System.out.println(PageFormate.wrongChoice + "\n");
		}
			
		}
	}
	
	
	public static void bjhbjhviewAppointments(int doctorId) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);

		System.out.println(PageFormate.pageSeprater);
		System.out.println("---------------------------- APPOINTMENT VIEW PAGE ----------------------------");

		while(true) {
			
		try {
				
		List<String> apps = AppointmentOperation.getAppointment(doctorId);
		
		if(apps == null) {
			return;
		}
		
		if (apps.size() == 0) {
			System.out.println("\n-------------------------- No Appointment scheduled ----------------------------------");
			return;
		}
		
		int count = 0;
		System.out.println("\n------> Enter your choice : \n");
		System.out.println("Entet 0 for retruning to Doctor Home Page :");

		for(String patientId : apps) {
			count++;
			System.out.println("Enter "+ count + " to create prescription for patient at ID " + apps.get(count-1));
		}
		
		int choice = Integer.parseInt(scan.nextLine());
		
		if(choice == 0) {
			DoctorHomePage.welcomeDoctor(doctorId);
			return;
		}
		
		choice = choice - 1;
		Prescription.createPrescription(doctorId,Integer.parseInt(apps.get(choice)));
		break;
		
		}catch(NumberFormatException e) {
			System.out.println(PageFormate.invalidInput + "\n");
		}catch(IndexOutOfBoundsException e) {
			System.out.println(PageFormate.wrongChoice + "\n");
		}
			
		}
	}
	
}
