package com.divergentsl.cms.doctor;

import java.util.Scanner;

import com.divergentsl.cms.dao.AppointmentOperation;
import com.divergentsl.cms.dao.DoctorOperations;
import com.divergentsl.cms.dto.Appointment;
import com.divergentsl.cms.login.Loign;
import com.divergentsl.cms.utils.PageFormate;

public class DoctorHomePage {
	

public static void authenticateDoctor() {
		
		System.out.println(PageFormate.pageSeprater);
		System.out.println("---------------------------- DOCTOR AUTHENTICATION -----------------------------\n");

		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter 0 for gong back to login module : ");
		System.out.println("Enter your doctor Id to view your appointments :");
		int doctorId = 0;
		
		try {
			doctorId = Integer.parseInt(scan.nextLine());
		}catch (NumberFormatException e) {
			// TODO: handle exception
			System.out.println(PageFormate.invalidInput);
			authenticateDoctor();
		}
		
		if (doctorId == 0) {
			Loign.showLoginMenu();
			return;
		}
		
		if(!DoctorOperations.checkExistance(doctorId)) {
			System.out.println("Wrong doctor ID try again");
			authenticateDoctor();
		}
			
		welcomeDoctor(doctorId);
	}

	public static void welcomeDoctor(int doctorId) {
		
		while(true) {
		try{
			
		System.out.println(PageFormate.pageSeprater);
		System.out.println("---------------------------- WELCOME TO DOCTOR HOME PAGE -----------------------------");
		
		System.out.println("\n------> Enter your choice :\n");
		
		System.out.println("Enter 1 to see the appointmetns :");
		System.out.println("Enter 2 to go back to login module :");
		System.out.println("Enter 3 to exit the application :");
		
		Scanner scan = new Scanner(System.in);
		int choice = Integer.parseInt(scan.nextLine());
		
		switch(choice) {
		
		case(1):
			Appointment.getAppointmentInfo(doctorId);
			break;
		
		case(2):
			Loign.showLoginMenu();
			break;
	
		case(3):
			System.out.println(PageFormate.exitGreet);
			System.exit(1);
		
		default :
			System.out.println(PageFormate.wrongChoice);
		}
		
		}catch(NumberFormatException e) {
			System.out.println(PageFormate.invalidInput);
		}
		}
	}
}
