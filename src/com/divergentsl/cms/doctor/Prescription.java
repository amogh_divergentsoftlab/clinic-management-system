package com.divergentsl.cms.doctor;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Scanner;

import com.divergentsl.cms.dao.DrugsOperation;
import com.divergentsl.cms.dao.LabtestOperations;
import com.divergentsl.cms.dao.PatientOperation;
import com.divergentsl.cms.dao.PrescriptionOperation;
import com.divergentsl.cms.utils.PageFormate;
import com.divergentsl.cms.dto.Patient;
public class Prescription {

	
		public static void createPrescription(int doctorId, int patientId) {
			// TODO Auto-generated method stub
			
			System.out.println(PageFormate.pageSeprater);
			System.out.println("\n--------------------------- CREATE PRESCRIPTION PAGE -----------------------------\n");
			
			Patient.getPatientInfo(patientId);

			System.out.println("\nDoctor ID : " +  doctorId);
			
			Scanner scan = new Scanner(System.in);
			
			System.out.println("Write prescription :");
			
			String prescriptioin = scan.nextLine();
			
			System.out.println("Enter labtest for Patient (labtest ID) : ");
			int labtestId = Integer.parseInt(scan.nextLine());
			
			if(!LabtestOperations.checkExistance(labtestId))
				return;
			
			System.out.println("Enter the Drugs for patient (Drug ID) : ");
			int drugId = Integer.parseInt(scan.nextLine());
			
			if(!DrugsOperation.checkExistance(drugId))
				return;
			
			boolean check = PrescriptionOperation.registerPrescription(doctorId, patientId, labtestId, drugId, prescriptioin);
			
			if (check)
				System.out.println("----------------------- Prescription Registered -------------------------------");
			else
				System.out.println("----------------------- Prescription not Registered -------------------------------");
			}
	
}
