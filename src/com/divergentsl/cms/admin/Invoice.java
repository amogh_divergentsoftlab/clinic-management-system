package com.divergentsl.cms.admin;

import java.util.Scanner;

import com.divergentsl.cms.dao.InvoiceOperations;
import com.divergentsl.cms.dao.PatientOperation;
import com.divergentsl.cms.utils.PageFormate;

public class Invoice {

	public static void invoiceMangement() {
		
		System.out.println(PageFormate.pageSeprater);
		System.out.println("\n-------------------------------- Invoice Generation Page ---------------------------\n");
		
		System.out.println("Enter the pateint Id to generate invoice : ");
		try {
			
		Scanner scan = new Scanner(System.in);
		int patientId = Integer.parseInt(scan.nextLine());
		
		if(!PatientOperation.chcekExistance(patientId)) {
			System.out.println(PageFormate.pageSeprater);
			return;
		}
		
		if(InvoiceOperations.createInvoice(patientId))
			System.out.println("-------------------------------- Invoice Successfully created ------------------------------");
		else
			System.out.println("-------------------------------- Invoice not created ------------------------------");
		
			
		System.out.println(PageFormate.pageSeprater);
		
		}catch(NumberFormatException e) {
			System.out.println(PageFormate.invalidInput);
		}
	}
}
