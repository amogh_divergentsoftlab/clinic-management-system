package com.divergentsl.cms.admin;

import java.util.Scanner;

import com.divergentsl.cms.dao.AppointmentOperation;
import com.divergentsl.cms.dao.DoctorOperations;
import com.divergentsl.cms.dao.PatientOperation;
import com.divergentsl.cms.utils.PageFormate;

public class Appointment {

	public static void bookAppointment() {
		
		System.out.println(PageFormate.pageSeprater);
		System.out.println("\n--------------------- Appointment Page ---------------------\n");
		
		System.out.println("------> Enter the following details for booking an appointment : \n");
		System.out.println("Enter Doctor ID :");
		
		try {
			
		Scanner scan = new Scanner(System.in);
		int doctorId = Integer.parseInt(scan.nextLine());
		
		System.out.println("Enter Patient ID :");
		int patientId = Integer.parseInt(scan.nextLine());
		
		if(!DoctorOperations.checkExistance(doctorId) || !PatientOperation.chcekExistance(patientId))	{
			return;
		}
		
		String time = setTime();
		
		String date = setDate();
		
		boolean check = AppointmentOperation.createAppointment(doctorId,patientId,time,date);
		
		if(check)
			System.out.println("-------------------- Appointment Booked --------------------------");
		else
			System.out.println("-------------------- Appointment Not Booked --------------------------");	
			
		AdminHomePage.welcomeAdmin();
		
		}catch(NumberFormatException e) {
			System.out.println(PageFormate.invalidInput);
		}
		
	}
	
	public static String setTime() {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter time of Appointment (HH:MM) :");
		
		String time = scan.nextLine();
		
		if(time.charAt(2) != '/' || time.length() != 5) {
			System.out.println("Please enter the right time : ");
			time = setTime();
			
		}
		
		return time;
	}
	
	public static String setDate() {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter the date of Appointment (DD/MM/YYYY) :");
		String date = scan.nextLine();
		
		if(date.charAt(2) != '/' || date.charAt(5) != '/' || date.length() != 10) {
			System.out.println("Please enter the right time : ");
			date = setDate();
		}
		
		return date;
	}
}
