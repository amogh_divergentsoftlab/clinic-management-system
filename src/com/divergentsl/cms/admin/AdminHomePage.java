package com.divergentsl.cms.admin;

import java.util.InputMismatchException;
import java.util.Scanner;

import com.divergentsl.cms.login.Loign;
import com.divergentsl.cms.utils.PageFormate;

public class AdminHomePage {

	public static void welcomeAdmin() {
		
		System.out.println(PageFormate.pageSeprater);
		
		while (true) {
			
		try {
			
		System.out.println("\n------------------------ Welcome Admin ---------------------");
		System.out.println("\n------> Enter your choice :-\n");
		
		System.out.println("Enter 1 for CRUD opertation on Doctor :");
		System.out.println("Enter 2 for CRUD opertation on Patient :");
		System.out.println("Enter 3 for CRUD opertation on Drugs :");
		System.out.println("Enter 4 for CRUD opertation on lab Test :");
		System.out.println("Enter 5 for Booking an Appointment :");
		System.out.println("Enter 6 for Invoice management :");
		System.out.println("Enter 7 for going back in login Module :");
		System.out.println("Enter 8 for Exit :");
		
		Scanner scan = new Scanner(System.in);
		int choice = scan.nextInt();
		
		switch(choice) {
		
		case (1):
			CrudDoctor.crudDoctor();
			break;
		
		case (2):
			CrudPatient.crudPatient();
			break;
		
		case (3):
			CrudDrugs.crudDrugs();
			break;
		
		case (4):
			CrudLabTest.crudLabTest();
			break;
			
		case (5):
			Appointment.bookAppointment();
			break;
		
		case (6):
			Invoice.invoiceMangement();
			break;
			
		case (7):
			Loign.showLoginMenu();
			break;
		
		case (8):
			System.out.println(PageFormate.exitGreet);
			System.exit(1);
			
		default :
			System.out.println(PageFormate.wrongChoice);
			break;
		
		}
		}catch(InputMismatchException e) {
			System.out.println(PageFormate.invalidInput);
		}
	}
	}
}
