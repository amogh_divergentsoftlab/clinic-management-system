package com.divergentsl.cms.admin;


import java.util.InputMismatchException;
import java.util.Scanner;

import com.divergentsl.cms.dao.DoctorOperations;
import com.divergentsl.cms.dto.Doctor;
import com.divergentsl.cms.login.Loign;
import com.divergentsl.cms.utils.PageFormate;

public class CrudDoctor {

	static void crudDoctor() {
		
		while(true) {
			
			try { 
				
			CommonView.createViewTemplate("Doctor");
			
			Scanner scan = new Scanner(System.in);
			int choice = scan.nextInt();
			
			switch (choice) {
			
				case (1):
					createDoctor();
					break;
					
				case (2):
					viewDoctorDetails();
					break;
					
				case (3):
					updateDoctor();
					break;
					
				case (4):
					deleteDoctor();
					break;
				
				case (5):
					AdminHomePage.welcomeAdmin();
					break;
					
				case (6):
					Loign.showLoginMenu();
					break;
					
				case (7):
					System.out.println(PageFormate.exitGreet);
					System.exit(1);
					break;
					
				default :
					System.out.println(PageFormate.wrongChoice);
					break;
			}
			
			}catch(InputMismatchException e) {
				
				System.out.println(PageFormate.invalidInput);
			}catch(NumberFormatException e) {
				System.out.println(PageFormate.invalidInput);
			}
		
		}
	}
	
	private static void deleteDoctor(){
		// TODO Auto-generated method stub
		
		System.out.println(PageFormate.pageSeprater);
		System.out.println(CommonView.deletePageHeader("DOCTOR"));
		
		System.out.println("Enter the Doctor Id you want to delete form doctor table :");
		Scanner scan = new Scanner(System.in);
		
		int doctorID = Integer.parseInt(scan.nextLine());
		
		boolean check = DoctorOperations.deleteDoctor(doctorID);
				
		if (check)
			System.out.println("--------------------------Doctor deleted successfully --------------");
		else
			System.out.println("--------------------------Doctor not deleted --------------");

			
		CrudDoctor.crudDoctor();
		
	}

	private static void updateDoctor() {
		// TODO Auto-generated method stub
		
		System.out.println(PageFormate.pageSeprater);
		System.out.println(CommonView.updatePageHeader("DOCTOR"));
		
		System.out.println("Enter the Doctor Id you want to update the details : ");
		Scanner scan = new Scanner(System.in);
		
		int DoctorId = Integer.parseInt(scan.nextLine());
		
		if (!DoctorOperations.checkExistance(DoctorId))
			return;
		
		System.out.println("what do you want to update in doctor : ");
		
		System.out.println("Enter 1 for name :");
		System.out.println("Enter 2 for Age :");
		System.out.println("Enter 3 for degree :");
		System.out.println("Enter 4 for specialization :");
		System.out.println("Enter 5 for fee :");
	
		//Scanner scan = new Scanner(System.in);
		int choice = Integer.parseInt(scan.nextLine());
		
		String query = null;
		String placeholder = null;
		
		switch(choice) {
		
		case (1):
			System.out.println("Enter the new name of Doctor :");
			placeholder = scan.nextLine();
			query = "update doctor set Name = \"" + placeholder + "\" where Doctor_ID = " + DoctorId +"";
			break;
		
		case (2):
			System.out.println("Enter the new Age of Doctor :");
			placeholder = scan.nextLine();
			query = "update doctor set Age = \"" + placeholder + "\" where Doctor_ID = " + DoctorId +"";
			break;
			
		case (3):
			System.out.println("Enter the new Contact No. of Doctor :");
			placeholder = scan.nextLine();
			query = "update doctor set Contact_No = \"" + placeholder + "\" where Doctor_ID = " + DoctorId +"";
			break;
		
		case (4):
			System.out.println("Enter the new Specialization of Doctor :");
			placeholder = scan.nextLine();
			query = "update doctor set Specialization = \"" + placeholder + "\" where Doctor_ID = " + DoctorId +"";
			break;
		
		case (5):
			System.out.println("Enter the new Fee of Doctor :");	
			placeholder = scan.nextLine();
			query = "update doctor set Fee = \"" + placeholder + "\" where Doctor_ID = " + DoctorId +"";	
			break;
			
		default :
			System.out.println("you have entered a wrong choice : ");
			break;
		
		}

		
		boolean check =DoctorOperations.updateDoctor(query);
		
		if (check) {
			System.out.println("------------------------Updates registered successfully-----------------------------");
		}else{			
			System.out.println("------------------------Updates not "
					+ "registered -----------------------------");
		}
			
		
		CrudDoctor.crudDoctor();
	}

	private static void viewDoctorDetails() {
		// TODO Auto-generated method stub
		
		System.out.println(PageFormate.pageSeprater);
		System.out.println(CommonView.viewPageHeader("DOCTOR"));

		System.out.println("Enter the doctor Id to view details : ");
		Scanner scan = new Scanner(System.in);
		
		int doctorID = Integer.parseInt(scan.nextLine());
		Doctor.viewDoctorInfo(doctorID);
		//DoctorOperations.viewDoctorDetails(doctorID);
		
		CrudDoctor.crudDoctor();
		
	}
		
	private static void createDoctor() {
		// TODO Auto-generated method stub
		
		Scanner scan = new Scanner(System.in);
		System.out.println(PageFormate.pageSeprater);
		System.out.println(CommonView.createPageHeader("DOCTOR"));

		System.out.println("Enter the given details for creating doctor :");
		
		System.out.println("Enter the name :  ");
		String name = scan.nextLine();
		
		System.out.println("Enter the age:");
		int age = Integer.parseInt(scan.nextLine());
		
		System.out.println("Enter the contact No. :");
		long contactNo = Long.parseLong(scan.nextLine());
		
		System.out.println("Enter the specilisation :");
		String specialization = scan.nextLine();
		
		System.out.println("Enter the Degree :");
		String degree = scan.nextLine();
		
		System.out.println("Enter the fee :");
		float fee = Float.parseFloat(scan.nextLine());
		
		DoctorOperations.createDoctor(name,age, contactNo, specialization, degree, fee);
		
		System.out.println("------------------- Doctor Successfully created --------------");
		
		CrudDoctor.crudDoctor();  
		
	}
	
}
