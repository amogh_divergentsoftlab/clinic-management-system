package com.divergentsl.cms.admin;

import java.util.InputMismatchException;
import java.util.Scanner;

import com.divergentsl.cms.dao.PatientOperation;
import com.divergentsl.cms.dto.Patient;
import com.divergentsl.cms.login.Loign;
import com.divergentsl.cms.utils.PageFormate;


public class CrudPatient {

	static void  crudPatient() {
		
		while(true) {
			
			try {
				
			CommonView.createViewTemplate("Patient");
			
			Scanner scan = new Scanner(System.in);
			int choice = scan.nextInt();
			
			switch (choice) {
			
				case (1):
					createPatient();
					break;
					
				case (2):
					viewPatient();
					break;
				
				case (3):
					updatePatient();
					break;
				
				case (4):
					deletePatient();
					break;
				
				case (5):
					AdminHomePage.welcomeAdmin();
					break;
					
				case (6):
					Loign.showLoginMenu();
					break;
					
				case (7):
					System.out.println(PageFormate.exitGreet);
					System.exit(1);
					
				default :
					System.out.println(PageFormate.wrongChoice);
					break;
			}
			
			}catch(InputMismatchException e) {
				System.out.println(PageFormate.invalidInput);
			}catch(NumberFormatException e) {
				System.out.println(PageFormate.invalidInput);
			}
		
		}


	}

	private static void deletePatient() {
		// TODO Auto-generated method stub
		
		System.out.println(PageFormate.pageSeprater);
		System.out.println(CommonView.deletePageHeader("PATIENT"));

		System.out.println("Enter the patient ID to be deleted :");
		Scanner scan = new Scanner(System.in);
		
		int patientID = Integer.parseInt(scan.nextLine());
		
		boolean check = PatientOperation.deletePatient(patientID);
		
		if(check)
			System.out.println("-------------------------- Patient deleted successfully --------------");
		else
			System.out.println("-------------------------- Patient not deleted --------------");

		
		CrudPatient.crudPatient();
		
	}

	private static void updatePatient() {
		// TODO Auto-generated method stub

		System.out.println(PageFormate.pageSeprater);
		System.out.println(CommonView.updatePageHeader("PATIENT"));

		System.out.println("Enter the patient ID to update record :");
		Scanner scan = new Scanner(System.in);
		
		int patientID = Integer.parseInt(scan.nextLine());
		
		if (!PatientOperation.chcekExistance(patientID))
			return;
		
		System.out.println("what do you want to update in patient : ");
		
		System.out.println("Enter 1 for name     :");
		System.out.println("Enter 2 for age      :");
		System.out.println("Enter 3 for contact  :");
		System.out.println("Enter 4 for gender   :");
		System.out.println("Enter 5 for disease  :");
		System.out.println("Enter 6 for status   :");
	
		//Scanner scan = new Scanner(System.in);
		int choice = Integer.parseInt(scan.nextLine());
		
		String query = null;
		String placeholder = null;
		
		switch(choice) {
		
		case (1):
			System.out.println("Enter the new name of patient :");
			placeholder = scan.nextLine();
			query = "update patient set Name = \"" + placeholder + "\" where Patient_ID = " + patientID +"";
			break;
		
		case (2):
			System.out.println("Enter the new Age of Patient :");
			placeholder = scan.nextLine();
			query = "update patient set Age = \"" + placeholder + "\" where Patient_ID = " + patientID +"";
			break;
			
		case (3):
			System.out.println("Enter the new Contact No. of Patient :");
			placeholder = scan.nextLine();
			query = "update patient set Contact_No = \"" + placeholder + "\" where Patient_ID = " + patientID +"";
			break;
		
		case (4):
			System.out.println("Enter the new Gender of Patient :");
			placeholder = selectGender();
			query = "update patient set Gender = \"" + placeholder + "\" where Patient_ID = " + patientID +"";
			break;
		
		case (5):
			System.out.println("Enter the new disease of Patient :");	
			placeholder = scan.nextLine();
			query = "update patient set Disease = \"" + placeholder + "\" where Patient_ID = " + patientID +"";	
			break;
			
		case (6):
			System.out.println("Enter the new status of Patient :");	
			placeholder = scan.nextLine();
			query = "update patient set Status = \"" + placeholder + "\" where Patient_ID = " + patientID +"";	
			break;
			
		default :
			
			System.out.println(PageFormate.wrongChoice);
			break;
		
		}

		
		boolean check = PatientOperation.updatePateint(query);
		
		if(check)
			System.out.println("------------------------Updates registered successfully-----------------------------");
		else
			System.out.println("------------------------Updates not registered -----------------------------");

		CrudPatient.crudPatient();
		
	}

	private static void viewPatient() {
		// TODO Auto-generated method stub
		
		System.out.println(PageFormate.pageSeprater);
		System.out.println(CommonView.viewPageHeader("PATIENT"));

		System.out.println("Enter Patient ID to view pateint details :");
		Scanner scan = new Scanner(System.in);
		
		int patientID = Integer.parseInt(scan.nextLine());
		
		Patient.getPatientInfo(patientID);
		//PatientOperation.viewPatientDetails(patientID);

		CrudPatient.crudPatient();
		
	}

	private static void createPatient() {
		// TODO Auto-generated method stub
		
		Scanner scan = new Scanner(System.in);
		System.out.println(PageFormate.pageSeprater);
		System.out.println(CommonView.createPageHeader("PATIENT"));
		
		System.out.println("\nEnter the given details for creating patient :");
		
		System.out.println("\nEnter the name :  ");
		String name = scan.nextLine();
		
		System.out.println("\nEnter the age :");
		String age = scan.nextLine();
		
		System.out.println("\nEnter the contact No. :");
		String contactNo = scan.nextLine();
		
		//System.out.println("Enter the gender :");
		String gender = selectGender();
		
		System.out.println("\nEnter the Deases :");
		String diseases = scan.nextLine();
		
		System.out.println("\nEnter the status :");
		String status = scan.nextLine();
		
		PatientOperation.createPatient(name,Integer.parseInt(age),Long.parseLong(contactNo),gender,diseases,status);
		
		System.out.println("------------------- Patient Successfully created --------------");
		
		CrudPatient.crudPatient();
		
	}
	
	public static String selectGender() {
		
		System.out.println("\nselect gender : \n");
		
		System.out.println("Enter 1 for male :");
		System.out.println("Enter 2 for female :");
		System.out.println("Enter 3 for others :");
		
		Scanner scan = new Scanner(System.in);
		int gender = 0;
		try {
		gender = Integer.parseInt(scan.nextLine());
		} catch(NumberFormatException e) {
			System.out.println(PageFormate.invalidInput);
			 return selectGender();
		}
		
		if (gender == 1) 
			return "male";
			
		else if(gender == 2)
			return "female";
		
		else if( gender == 3)
			return "others";
		
		else {
			System.out.println("invlaid choice please try again : ");
			selectGender();
		}
			
			return null;
	}
}
