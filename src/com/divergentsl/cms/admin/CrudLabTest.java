package com.divergentsl.cms.admin;

import java.util.InputMismatchException;
import java.util.Scanner;

import com.divergentsl.cms.dao.DoctorOperations;
import com.divergentsl.cms.dao.DrugsOperation;
import com.divergentsl.cms.dao.LabtestOperations;
import com.divergentsl.cms.dto.Labtest;
import com.divergentsl.cms.login.Loign;
import com.divergentsl.cms.utils.PageFormate;

public class CrudLabTest {

	static void crudLabTest() {
				
		while(true) {
			
			try {
			
			CommonView.createViewTemplate("Lab Test");
			
			Scanner scan = new Scanner(System.in);
			int choice = scan.nextInt();
			
			switch (choice) {
			
				case (1):
					createLabTest();
					break;
					
				case (2):
					viewLabTest();
					break;
				
				case (3):
					updateLabTest();
					break;
				
				case (4):
					deleteLabTest();
					break;
				
				case (5):
					AdminHomePage.welcomeAdmin();
					break;
					
				case (6):
					Loign.showLoginMenu();
					break;
					
				case (7):
					System.out.println(PageFormate.exitGreet);
					System.exit(1);
					
				default :
					System.out.println(PageFormate.wrongChoice);
					break;
			}
		
			}catch(InputMismatchException e) {
				System.out.println(PageFormate.invalidInput);
			}catch(NumberFormatException e) {
				System.out.println(PageFormate.invalidInput);
			}
		}

		
	}

	private static void deleteLabTest() {
		// TODO Auto-generated method stub

		System.out.println(PageFormate.pageSeprater);
		System.out.println(CommonView.deletePageHeader("LABTEST"));

		System.out.println("Enter the labtest ID for deletion");
		Scanner scan = new Scanner(System.in);
		
		int labtestId = Integer.parseInt(scan.nextLine());
		
		boolean check = LabtestOperations.deleteLabtest(labtestId);
		
		if(check)
			System.out.println("-------------------------- Labtest deleted successfully ---------------------------");
		else
			System.out.println("-------------------------- Labtest not deleted ------------------------------");
		
		CrudLabTest.crudLabTest();
		
		
	}

	private static void updateLabTest() {
		// TODO Auto-generated method stub
		
		System.out.println(PageFormate.pageSeprater);
		System.out.println(CommonView.updatePageHeader("LABTEST"));

		System.out.println("Enter the labtest ID for update");
		Scanner scan = new Scanner(System.in);
		
		int labtestId = Integer.parseInt(scan.nextLine());
		
		if (!LabtestOperations.checkExistance(labtestId))
			return;
		
		System.out.println("what do you want to update in doctor : ");
		
		System.out.println("Enter 1 for update doctor :");
		System.out.println("Enter 2 for update labtest for :");
		System.out.println("Enter 3 for update charges :");
	
		//Scanner scan = new Scanner(System.in);
		int choice = Integer.parseInt(scan.nextLine());
		
		String query = null;
		String placeholder = null;
		
		switch(choice) {
		
		case (1):
			System.out.println("Enter the new doctor for labtest :");
			placeholder = scan.nextLine();
			query = "update labtest set Doctor_ID = \"" + placeholder + "\" where Labtest_ID = " + labtestId +"";
			break;
		
		case (2):
			System.out.println("Enter the new labtest for :");
			placeholder = scan.nextLine();
			query = "update labtest set LabTestFor = \"" + placeholder + "\" where labtest_ID = " + labtestId +"";
			break;
			
		case (3):
			System.out.println("Enter the new charges for labtest :");
			placeholder = scan.nextLine();
			query = "update labtest set Charges = \"" + placeholder + "\" where labtest_ID = " + labtestId +"";
			break;
			
		default :
			System.out.println("you have entered a wrong choice : ");
			break;
		
		}

		boolean check = LabtestOperations.updateLabtest(query);
		
		if(check)
			System.out.println("------------------------ Updates registered successfully -----------------------------");
		else
			System.out.println("------------------------ Updates not registered -----------------------------");

		
		CrudLabTest.crudLabTest();
		
	}

	private static void viewLabTest() {
		// TODO Auto-generated method stub

		System.out.println(PageFormate.pageSeprater);
		System.out.println(CommonView.viewPageHeader("LABTEST"));

		System.out.println("Enter the labtest ID for View");
		Scanner scan = new Scanner(System.in);
		
		int labtestId = Integer.parseInt(scan.nextLine());
		
		Labtest.viewLabtestInfo(labtestId);
		//LabtestOperations.viewLabtest(labtestId);
		
		CrudLabTest.crudLabTest();
		
		
	}

	private static void createLabTest() {
		// TODO Auto-generated method stub
		
		Scanner scan = new Scanner(System.in);
		System.out.println(PageFormate.pageSeprater);
		System.out.println(CommonView.createPageHeader("LABTEST"));
		
		System.out.println("Enter the details for labtest : ");
		
		System.out.println("Enter the Doctor ID : ");
		int doctorId = Integer.parseInt(scan.nextLine());
		
		System.out.println("Enter the Pateint ID : ");
		int patientId = Integer.parseInt(scan.nextLine());
		
		System.out.println("lab test for :");
		String labtestfor = scan.nextLine();
		
		System.out.println("Enter the charges for labtest");
		float charges = Float.parseFloat(scan.nextLine());
		
		boolean check = LabtestOperations.createlabtest(doctorId, patientId, labtestfor, charges);
		
		if(check)
			System.out.println("---------------------------labtest successfully createed --------------------------------");
		else
			System.out.println("---------------------------labtest not successfully --------------------------------");

		CrudLabTest.crudLabTest();
		
	}
}
