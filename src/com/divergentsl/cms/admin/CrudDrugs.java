package com.divergentsl.cms.admin;

import java.util.InputMismatchException;
import java.util.Scanner;

import com.divergentsl.cms.dao.DrugsOperation;
import com.divergentsl.cms.dto.Drugs;
import com.divergentsl.cms.utils.PageFormate;
import com.divergentsl.cms.login.Loign;

public class CrudDrugs {

	static void crudDrugs() {
			
		while(true) {
			
			try {
				
			CommonView.createViewTemplate("Drugs");
			
			Scanner scan = new Scanner(System.in);
			int choice = scan.nextInt();
			
			switch (choice) {
			
				case (1):
					createDrugs();
					break;
					
				case (2):
					viewDrugs();
					break;
				
				case (3):
					updateDrugs();
					break;
				
				case (4):
					deleteDrugs();
					break;
				
				case (5):
					AdminHomePage.welcomeAdmin();
					break;
					
				case (6):
					Loign.showLoginMenu();
					break;
					
				case (7):
					System.out.println(PageFormate.exitGreet);
					System.exit(1);
					
				default :
					System.out.println(PageFormate.wrongChoice);
					break;
			}
			
			}catch(InputMismatchException e) {
				System.out.println(PageFormate.invalidInput);
			}catch(NumberFormatException e) {
				System.out.println(PageFormate.invalidInput);
			}
		
		}

		
	}

	private static void deleteDrugs() {
		// TODO Auto-generated method stub
		
		System.out.println(PageFormate.pageSeprater);
		System.out.println(CommonView.deletePageHeader("DRUGS"));
		
		System.out.println("Enter the Drugs Id to be delted :");
		Scanner scan = new Scanner(System.in);
		
		int drugsId = Integer.parseInt(scan.nextLine());
		
		boolean check =DrugsOperation.delteDrugs(drugsId);
		
		if (check) {
			System.out.println("--------------------------Drug deleted successfully --------------");
		}else {
			System.out.println("--------------------------Drug not deleted  --------------");
		}
		
		CrudDrugs.crudDrugs();
		
	}

	private static void updateDrugs() {
		// TODO Auto-generated method stub
		
		System.out.println(PageFormate.pageSeprater);
		System.out.println(CommonView.updatePageHeader("DRUGS"));
		
		System.out.println("Enter the Drugs Id to be updated :");
		Scanner scan = new Scanner(System.in);
		
		int drugsId = Integer.parseInt(scan.nextLine());
		
		if(!DrugsOperation.checkExistance(drugsId))
			return;
		
		System.out.println("what do you want to update in drug " +  drugsId +" : ");
		
		System.out.println("Enter 1 for name               :");
		System.out.println("Enter 2 for manufacturing date :");
		System.out.println("Enter 3 for expiry date        :");
		System.out.println("Enter 4 for charges            :");
	
		//Scanner scan = new Scanner(System.in);
		int choice = Integer.parseInt(scan.nextLine());
		
		String query = null;
		String placeholder = null;
		
		switch(choice) {
		
		case (1):
			System.out.println("Enter the new name of Drug :");
			placeholder = scan.nextLine();
			query = "update drugs set Name = \"" + placeholder + "\" where Drug_ID = " + drugsId +";";
			break;
		
		case (2):{
			System.out.println("Enter the new manufacturing date of Drug :");
			System.out.println("Enter the month :");
			String month = scan.nextLine();
			
			System.out.println("Enter the year :");
			String year = scan.nextLine();
			
			placeholder = month + "/" + year;
			query = "update drugs set ManufacturingDate = \"" + placeholder + "\" where Drug_ID = " + drugsId +";";
			break;
		}
		case (3):{
			System.out.println("Enter the new expiry date of Drug:");
			System.out.println("Enter the month :");
			String month1 = scan.nextLine();
			
			System.out.println("Enter the year :");
			String year1 = scan.nextLine();
			
			placeholder = month1 + "/" + year1;
			query = "update drugs set ExpiryDate = \"" + placeholder + "\" where Drug_ID = " + drugsId +";";
			break;
		}
		
		case (4):
			System.out.println("Enter the new Charges of drugs :");
			float charges = Float.parseFloat(scan.nextLine());
			query = "update drugs set Charges = \"" + charges + "\" where Drug_ID = " + drugsId +";";
			break;
			
		default :
			System.out.println("you have entered a wrong choice : ");
			break;
		
		}

		boolean check = DrugsOperation.updateDrugs(query);
		
		if(check)
			System.out.println("------------------------ Updates registered successfully -----------------------------");
		else
			System.out.println("------------------------ Updates not registered -----------------------------");

			
		System.out.println(PageFormate.pageSeprater);
		CrudDrugs.crudDrugs();
	}

	private static void viewDrugs() {
		// TODO Auto-generated method stub
		
		System.out.println(PageFormate.pageSeprater);
		System.out.println(CommonView.viewPageHeader("DRUGS"));
		
		System.out.println("Enter the Drugs Id to view drugs details :");
		Scanner scan = new Scanner(System.in);
		
		int drugsID = Integer.parseInt(scan.nextLine());
		
		//DrugsOperation.viewDrugs(drugsID);
		Drugs.getDrugsInfo(drugsID);
		
		CrudDrugs.crudDrugs();
	}

	private static void createDrugs() {
		// TODO Auto-generated method stub
		
		Scanner scan = new Scanner(System.in);
 		System.out.println(PageFormate.pageSeprater);
 		System.out.println(CommonView.createPageHeader("DRUGS"));
 		
		System.out.println("Enter the given details for creating a drug :");
		
		System.out.println("Enter the name :  ");
		String name = scan.nextLine();
		
		System.out.println("Enter the manufacturig date of drug :");
		
		System.out.println("Enter the month : ");
		int mMonth = Integer.parseInt(scan.nextLine());
		
		System.out.println("Enter the year :");
		int mYear = Integer.parseInt(scan.nextLine());
		
		String maufacturingDate = mMonth + "/" + mYear;
		
		
		System.out.println("Enter the expiry date of drug :");
		System.out.println("Enter the month :");
		int eMonth = Integer.parseInt(scan.nextLine());
		
		System.out.println("Enter the year :");
		int eYear = Integer.parseInt(scan.nextLine());
		
		String expiryDate = eMonth + "/" + eYear;
		
		System.out.println("Enter the charges for the drug :");
		float charges = Float.parseFloat(scan.nextLine());

		boolean check = DrugsOperation.createDrug(name,maufacturingDate,expiryDate,charges);
		
		if (check)
			System.out.println("--------------------Drug Created Successfully ------------------------");
		else
			System.out.println("--------------------Drug not Created ------------------------");
		
		CrudDrugs.crudDrugs();
	}
}
