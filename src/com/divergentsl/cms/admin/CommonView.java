package com.divergentsl.cms.admin;

import com.divergentsl.cms.utils.PageFormate;

public class CommonView {

	static void createViewTemplate(String user) {
		
		System.out.println(PageFormate.pageSeprater);
		System.out.println("\n--------------------------------- Select operation on " + user + " --------------------------------");

		System.out.println("\n------> Enter your choice : \n");
		
		System.out.println("Enter 1 for create " + user + " :");
		System.out.println("Enter 2 for View " + user +" details :");
		System.out.println("Enter 3 for update the " + user + " :");
		System.out.println("Enter 4 for delete the " + user  +" :");
		System.out.println("Enter 5 for going back to Admin home page :");
		System.out.println("Enter 6 for going back in login Module :");
		System.out.println("Enter 7 for Exit :");
		
	}
	
	public static String viewPageHeader(String reference) {
		return "\n--------------------- VIEW " + reference  + " PAGE -------------------------\n";
		
	}
	
	public static String createPageHeader(String reference) {
		return "\n--------------------- CREATE " + reference  + " PAGE -----------------------\n";

	}
	
	public static String deletePageHeader(String reference) {
		return "\n--------------------- DELETE " + reference  + " PAGE -----------------------\n";
	}
	
	public static String updatePageHeader(String reference) {
		return "\n--------------------- UPDATE " + reference  + " PAGE -----------------------\n";
	}
	
}
