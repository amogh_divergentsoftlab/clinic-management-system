package com.divergentsl.cms.dao;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.divergentsl.cms.dto.Drugs;
import com.divergentsl.cms.utils.ConnectionUtil;

public class DrugsOperation {
	
	public static boolean checkExistance(int drugId) {
		
		String que1 = "select exists(select * from drugs where Drug_ID = " + drugId + ");";
		
		try (java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
			
			ResultSet rs = statement.executeQuery(que1);
			
			int status = -1; 
			while(rs.next())
				status = rs.getInt(1);
			
			if (status == 0) {
				System.out.println("No Drugs exists with ID : " + drugId);
				return false;
			}
			
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	

	public static boolean delteDrugs(int drugsId) {
		// TODO Auto-generated method stub
		
		if(!checkExistance(drugsId))
			return false;
		
		String query = " delete from drugs where Drug_ID = " + drugsId + ";";
		
		try (java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
			
				statement.execute(query);
				return true;			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		return false;
	}

	public static boolean  updateDrugs(String query) {
		// TODO Auto-generated method stub
		
//		if(!checkExistance(drugsId))
//			return;
//		
//		System.out.println("what do you want to update in drug " +  drugsId +" : ");
//		
//		System.out.println("Enter 1 for name               :");
//		System.out.println("Enter 2 for manufacturing date :");
//		System.out.println("Enter 3 for expiry date        :");
//		System.out.println("Enter 4 for charges            :");
//	
//		Scanner scan = new Scanner(System.in);
//		int choice = Integer.parseInt(scan.nextLine());
//		
//		String query = null;
//		String placeholder = null;
//		
//		switch(choice) {
//		
//		case (1):
//			System.out.println("Enter the new name of Drug :");
//			placeholder = scan.nextLine();
//			query = "update drugs set Name = \"" + placeholder + "\" where Drug_ID = " + drugsId +";";
//			break;
//		
//		case (2):{
//			System.out.println("Enter the new manufacturing date of Drug :");
//			System.out.println("Enter the month :");
//			String month = scan.nextLine();
//			
//			System.out.println("Enter the year :");
//			String year = scan.nextLine();
//			
//			placeholder = month + "/" + year;
//			query = "update drugs set ManufacturingDate = \"" + placeholder + "\" where Drug_ID = " + drugsId +";";
//			break;
//		}
//		case (3):{
//			System.out.println("Enter the new expiry date of Drug:");
//			System.out.println("Enter the month :");
//			String month1 = scan.nextLine();
//			
//			System.out.println("Enter the year :");
//			String year1 = scan.nextLine();
//			
//			placeholder = month1 + "/" + year1;
//			query = "update drugs set ExpiryDate = \"" + placeholder + "\" where Drug_ID = " + drugsId +";";
//			break;
//		}
//		
//		case (4):
//			System.out.println("Enter the new Charges of drugs :");
//			float charges = Float.parseFloat(scan.nextLine());
//			query = "update drugs set Charges = \"" + charges + "\" where Drug_ID = " + drugsId +";";
//			break;
//			
//		default :
//			System.out.println("you have entered a wrong choice : ");
//			break;
//		
//		}
		
		try(java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
			
			
			statement.executeUpdate(query);
			return true;
						
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
		
	}

	public static Drugs viewDrugs(int drugsId) {
		// TODO Auto-generated method stub
		
		if(!checkExistance(drugsId))
			return null;
		
		try(java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
			
			String query = "select * from drugs where Drug_ID = " + drugsId+ ";";
			ResultSet rs = statement.executeQuery(query);
			
			while(rs.next()) {
							
				return new Drugs(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDouble(5));
			
				
//				System.out.println("Drug ID                 : " + rs.getString(1));
//				System.out.println("Drug Name               : " + rs.getString(2));
//				System.out.println("Drug manufacturing date : " + rs.getString(3));
//				System.out.println("Drug expiry date        : " + rs.getString(4));
//				System.out.println("Drrug Charges           : " + rs.getString(5));
			
			}	
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	
		
		return null;
	}


	public static boolean createDrug(String name, String manufacturingDate, String expiryDate, float charges) {
		// TODO Auto-generated method stub
	
		String query = " insert into drugs(name,ManufacturingDate,ExpiryDate,Charges)\r\n" + 
				"values (\"" + name +"\",\"" + manufacturingDate+"\",\"" + expiryDate+ "\", " + charges + ");";
		
		try (java.sql.Connection connection = ConnectionUtil.createConncetion();  
				Statement statement = connection.createStatement()){
			
			statement.execute(query);
			
			return true;
						
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}

	
}
