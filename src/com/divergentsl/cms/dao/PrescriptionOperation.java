package com.divergentsl.cms.dao;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import com.divergentsl.cms.dto.Prescription;

import javax.sql.rowset.JdbcRowSet;

import com.divergentsl.cms.utils.ConnectionUtil;
import com.divergentsl.cms.utils.RowSetUtil;

public class PrescriptionOperation {

	public static boolean registerPrescription(int doctorId, int patientId, int labtestId, int drugId,
			String prescriptioin) {
		// TODO Auto-generated method stub
		
		if (!LabtestOperations.checkExistance(labtestId) && !DrugsOperation.checkExistance(drugId)) {
			return false;
		}
		
		String query = "insert into prescription(Doctor_ID, Patient_ID, Drugs_ID, Labtest, Prescription) values\r\n" + 
				"(" + doctorId+ "," + patientId + "," + drugId + "," + labtestId + " ,\"" + prescriptioin +"\");";
		
		try(java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
			
			statement.execute(query);
			return true;			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}

	public static List<Prescription> viewPrescription(int patientId) {
		
		String query = "select * from prescription where patient_ID = " + patientId + " order by Prescription_ID desc;";
		
		try (JdbcRowSet rowSet = RowSetUtil.rowSetProvider();
			JdbcRowSet rs = RowSetUtil.rowSetProvider()){
			
			rowSet.setCommand(query); rs.setCommand(query);
			rowSet.execute(); rs.execute();
			
			if (!rs.next()) {
				return null;
			}
			rs.close();
			
			List<Prescription> prescriptions = new ArrayList<>();
			
			while(rowSet.next()) {
				
//				System.out.println("Prescription " + presCount + " : \n");
//				
//				System.out.println("Prescription Id   :- " + rowSet.getString(1) );	
//				
//				System.out.println("Patient Info      :- ");
//				PatientOperation.viewPatientDetails(patientId);
//				System.out.println("-----");
//				
//				System.out.println("Labtest Info      :- ");
//				LabtestOperations.viewLabtest(rowSet.getInt(3));
//				
//				System.out.println("-----");
//				System.out.println("Drugs Id          :- ");
//				DrugsOperation.viewDrugs(rowSet.getInt(4));
//				
//				System.out.println("-----");
//				System.out.println("Prescription      :- \n" + rowSet.getString(6) );	
				
				prescriptions.add(new Prescription(rowSet.getInt(1), rowSet.getInt(2), rowSet.getInt(3), rowSet.getInt(4), rowSet.getInt(5), rowSet.getString(6)));
				
			}
			
			return prescriptions;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
