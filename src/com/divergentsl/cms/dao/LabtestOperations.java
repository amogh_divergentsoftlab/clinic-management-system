package com.divergentsl.cms.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.divergentsl.cms.dto.Labtest;
import com.divergentsl.cms.utils.ConnectionUtil;

public class LabtestOperations {
	
	public static boolean checkExistance(int labtestId) {
		
		String que1 = "select exists(select * from labtest where LabTest_ID = " + labtestId + ");";
		
		try (Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
			
			ResultSet rs = statement.executeQuery(que1);
			
			int status = -1; 
			while(rs.next())
				status = rs.getInt(1);
			
			if (status == 0) {
				System.out.println("No Lab Test exists with ID : " + labtestId);
				return false;
			}
			
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}

	public static boolean deleteLabtest(int labtestId) {
		// TODO Auto-generated method stub
		
		if (!checkExistance(labtestId))
			return false;
		
		String query = " delete from labtest where LabTest_ID = " + labtestId + ";";
		
		try (java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
						
				statement.execute(query);
				return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}

	public static boolean updateLabtest(String query) {
		// TODO Auto-generated method stub
	
//		if (!checkExistance(labtestId))
//			return;
//		
//		System.out.println("what do you want to update in doctor : ");
//		
//		System.out.println("Enter 1 for update doctor :");
//		System.out.println("Enter 2 for update labtest for :");
//		System.out.println("Enter 3 for update charges :");
//	
//		Scanner scan = new Scanner(System.in);
//		int choice = Integer.parseInt(scan.nextLine());
//		
//		String query = null;
//		String placeholder = null;
//		
//		switch(choice) {
//		
//		case (1):
//			System.out.println("Enter the new doctor for labtest :");
//			placeholder = scan.nextLine();
//			query = "update labtest set Doctor_ID = \"" + placeholder + "\" where Labtest_ID = " + labtestId +"";
//			break;
//		
//		case (2):
//			System.out.println("Enter the new labtest for :");
//			placeholder = scan.nextLine();
//			query = "update labtest set LabTestFor = \"" + placeholder + "\" where labtest_ID = " + labtestId +"";
//			break;
//			
//		case (3):
//			System.out.println("Enter the new charges for labtest :");
//			placeholder = scan.nextLine();
//			query = "update labtest set Charges = \"" + placeholder + "\" where labtest_ID = " + labtestId +"";
//			break;
//			
//		default :
//			System.out.println("you have entered a wrong choice : ");
//			break;
//		
//		}
		
		try(java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
			
			statement.executeUpdate(query);
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
		
	}

	public static Labtest viewLabtest(int labtestId) {
		// TODO Auto-generated method stub
		
		if (!checkExistance(labtestId))
			return null;
		
		try(java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
			
			String query = "select * from labtest where LabTest_ID = " + labtestId+ ";";
			ResultSet rs = statement.executeQuery(query);
			
			while(rs.next()) {
								
				return new Labtest(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getDouble(5));
				
//				System.out.println("Labtest ID          : " + rs.getString(1));
//				System.out.println("Labtest Patient ID  : " + rs.getString(2));
//				System.out.println("Labtest Doctor ID   : " + rs.getString(3));
//				System.out.println("Labtest Labtest for : " + rs.getString(4));
//				System.out.println("Labtest Charges     : " + rs.getString(5));
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	public static boolean createlabtest(int doctorId, int patientId, String labtestfor, float charges) {
		// TODO Auto-generated method stub
		
		try(java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
			
			if(!DoctorOperations.checkExistance(doctorId) && !PatientOperation.chcekExistance(patientId))	{
				//System.out.println("Either Doctor-ID or Patient-ID is invalid ");
				return false;
			}
			
			String query = "insert into labtest(Patient_ID, Doctor_ID, labTestFor, Charges) values"
					+ " ("+ patientId +"," + doctorId + ",\"" + labtestfor +"\"," + charges + ");";
		
			statement.execute(query);
			return true;			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}

	
}
