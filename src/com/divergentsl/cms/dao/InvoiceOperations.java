package com.divergentsl.cms.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.JdbcRowSet;
import com.divergentsl.cms.dto.Invocie;
import com.divergentsl.cms.utils.PageFormate;
import com.divergentsl.cms.utils.RowSetUtil;

public class InvoiceOperations {

	public static boolean createInvoice(int patientId) {
		// TODO Auto-generated method stub
		
		float doctorCharges = getCharges(patientId,"Doctor_ID","Fee","doctor","Doctor_ID");
		
		float labtestCharges = getCharges(patientId,"labtest","Charges","labtest","LabTest_ID");
		
		float drugsCharges = getCharges(patientId,"Drugs_ID","Charges","drugs","Drug_ID");

		float totalCharges  = doctorCharges + drugsCharges + labtestCharges;
		
		int invoiceId = registerInvoice(patientId, doctorCharges, labtestCharges, drugsCharges, totalCharges);
		
		if(invoiceId !=0)
			return true;
		else
			return false;
		
	}

	public static List<Invocie> viewInvoice(int patientId) {
		// TODO Auto-generated method stub
		
		try(JdbcRowSet rowSet = RowSetUtil.rowSetProvider();
				JdbcRowSet rs = RowSetUtil.rowSetProvider()){
			
			String query = "select * from invoice where Patient_ID = " + patientId +" order by Invoice_ID";
			List<Invocie> invoices = new ArrayList<>();
			
			rowSet.setCommand(query); rs.setCommand(query);
			rowSet.execute();  rs.execute();
			
			if(!rs.next() || !PatientOperation.chcekExistance(patientId)) {
				//System.out.println("No invoice for patient ID : " + patientId);
				return null;
			}
			
			while(rowSet.next()) {
				invoices.add(new Invocie(rowSet.getInt(1), rowSet.getInt(2), rowSet.getDouble(3), rowSet.getDouble(4), rowSet.getDouble(5), rowSet.getDouble(6)));
			}
			
			return invoices;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private static int registerInvoice(int patientId, float doctorCharges, float drugsCharges, float labtestCharges , float totalCharges) {
		// TODO Auto-generated method stub
	
		try(JdbcRowSet rowSet = RowSetUtil.rowSetProvider(); 
			JdbcRowSet rowSet1 = RowSetUtil.rowSetProvider()){
		
			int invoiceId = 0;
			
			String query = "select * from invoice";
			rowSet.setCommand(query);
			rowSet.execute();
			
			rowSet.moveToInsertRow();
			
			rowSet.updateString("Patient_ID", "" + patientId);
			rowSet.updateString("Doctor_Charges","" + doctorCharges);
			rowSet.updateString("labtest_Charges", "" + labtestCharges);
			rowSet.updateString("Drugs_Charges","" + drugsCharges);
			rowSet.updateString("Total_Amount", "" + totalCharges);
			
			rowSet.insertRow();
			
			String getInvoiceId = "select Invoice_ID from invoice order by Invoice_ID desc limit 1;";
			rowSet.setCommand(getInvoiceId);
			rowSet.execute();
			
			while(rowSet.next())
				invoiceId = rowSet.getInt(1);
				
			return invoiceId;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}	
	
	private static float getCharges(int patientId,String ...param) {
		
		float totalCharges = 0.0f;

		String query = "select " + param[0] + " from prescription where Patient_ID = " + patientId + " order by Prescription_ID DESC;";

		try (JdbcRowSet rowSet = RowSetUtil.rowSetProvider()){
			
			rowSet.setCommand(query);
			rowSet.execute();
			List<Integer> drugsIds = new ArrayList<>();
			
			while(rowSet.next())
				drugsIds.add(Integer.parseInt(rowSet.getString(1)));
			
			for(Integer i : drugsIds) {
				
				String query1 = "select " + param[1]  + " from " + param[2] + "  where " +  param[3] +  " = " + i + ";";
				try(JdbcRowSet rset = RowSetUtil.rowSetProvider()){
					
					rset.setCommand(query1);
					rset.execute();
					
					while(rset.next()) {
						if(rset.getString(1) == null)
							return 0.0f;
						
						totalCharges += Float.parseFloat(rset.getString(1));
					}
				}
			}
			
			return totalCharges;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 0;
		
	}

}
