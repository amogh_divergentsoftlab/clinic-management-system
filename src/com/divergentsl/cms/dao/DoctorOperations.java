package com.divergentsl.cms.dao;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Scanner;

import com.divergentsl.cms.dto.Doctor;
import com.divergentsl.cms.utils.ConnectionUtil;

public class DoctorOperations {
	
	

	public static void createDoctor(String name, int age, long contactNo, String specialization, String degree, float fee){
		
		try(java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
			
			String query = "insert into Doctor(Name,Age,Contact_No,Specialization,Degree,Fee) values" + 
					"(\""+ name +"\","+ age +","+ contactNo +",\""+ specialization + "\",\""+ degree +"\"," +fee +");";
			
		
			statement.execute(query);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public static boolean checkExistance(int doctorId) {
		
		String que1 = "select exists(select * from doctor where Doctor_ID = " + doctorId + ");";
		
		try (java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
			
			ResultSet rs = statement.executeQuery(que1);
			
			int status = -1; 
			while(rs.next())
				status = rs.getInt(1);
			
			if (status == 0) {
				System.out.println("No doctor exists with ID : " + doctorId);
				return false;
			}
			
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	
	public static boolean deleteDoctor(int doctorId) {
		
		
		if (!checkExistance(doctorId)) 
			return false;
		
		String query = " delete from doctor where Doctor_ID = " + doctorId + ";";
		
		try (java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
			
			
				statement.execute(query);
				
				return true;
		
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
		
	}
	 
	
	
	public static boolean updateDoctor(String query) {
		
		try(java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
			
			statement.executeUpdate(query);
			
			return true;
						
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
		
	}
	
	
//	public static void view1DoctorDetails(int doctorID) {
//	
//		if (!checkExistance(doctorID)) 
//			return;
//		
//		try(java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
//			
//				String query = "select * from doctor where Doctor_ID = " + doctorID+ ";";
//				ResultSet rs = statement.executeQuery(query);
//				Doctor doc = null;
//				
//				while(rs.next()) {
//					
//							
//					System.out.println("Doctor ID              : " + rs.getString(1));
//					System.out.println("Doctor Name            : " + rs.getString(2));
//					System.out.println("Doctor Age 	           : " + rs.getString(3));
//					System.out.println("Doctor Contact No.     : " + rs.getString(4));
//					System.out.println("Doctor Specialization  : " + rs.getString(5));
//					System.out.println("Doctor Degree          : " + rs.getString(6));
//					System.out.println("Doctor Fee	           : " + rs.getString(7));
//
//				}		
//			
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

public static Doctor viewDoctorDetails(int doctorID) {
	
	Doctor doctor = null;
	if (!checkExistance(doctorID)) 
		return null; 
	
	try(java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
		
			String query = "select * from doctor where Doctor_ID = " + doctorID+ ";";
			ResultSet rs = statement.executeQuery(query);
			Doctor doc = null;
			
			while(rs.next()) {
				
				return new Doctor(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getLong(4), rs.getString(5), rs.getString(6), rs.getFloat(7));
						
//				System.out.println("Doctor ID              : " + rs.getString(1));
//				System.out.println("Doctor Name            : " + rs.getString(2));
//				System.out.println("Doctor Age 	           : " + rs.getString(3));
//				System.out.println("Doctor Contact No.     : " + rs.getString(4));
//				System.out.println("Doctor Specialization  : " + rs.getString(5));
//				System.out.println("Doctor Degree          : " + rs.getString(6));
//				System.out.println("Doctor Fee	           : " + rs.getString(7));

			}		
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return null;
}
}

