package com.divergentsl.cms.dao;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.divergentsl.cms.dto.Patient;
import com.divergentsl.cms.utils.ConnectionUtil;
import com.divergentsl.cms.utils.PageFormate;

public class PatientOperation {

	public static boolean deletePatient(int patientID) {
		// TODO Auto-generated method stub
				
		if(!chcekExistance(patientID))
			return false;
		
		String query = " delete from patient where Patient_ID = " + patientID + ";";

		try (java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
		
			statement.execute(query);
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;	
	}

	public static boolean chcekExistance(int patientID) {
		
	String que1 = "select exists(select * from patient where Patient_ID = " + patientID + ");";
		
		try (java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
			
			ResultSet rs = statement.executeQuery(que1);
			
			int status = -1; 
			while(rs.next())
				status = rs.getInt(1);
			
			if (status == 0) {
				System.out.println("No Patient exists with ID : " + patientID);
				return false;
			}
			
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}

	public static void createPatient(String name, int age, long contactNo, String gender, String diseases,
			String status) {
		// TODO Auto-generated method stub
		
		try(java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
			
			String query = "insert into patient(Name,Age,Gender,Contact_No,Disease,Status) values\r\n" + 
					"(\"" + name + "\"," + age + ",\""+ gender +"\","+ contactNo +",\"" + diseases+ "\",\"" + status + "\");";
		
			statement.execute(query);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public static Patient viewPatientDetails(int patientID) {
		
		if (!chcekExistance(patientID))
			return null;
		
		try(java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
			
			String query = "select * from patient where Patient_ID = " + patientID+ ";";
			ResultSet rs = statement.executeQuery(query);
			while(rs.next()) {
				
				return new Patient(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getLong(5), rs.getString(6), rs.getString(7));
				
//				System.out.println("Patient ID          : "  + rs.getString(1));
//				System.out.println("Patient name        : "  + rs.getString(2));
//				System.out.println("Patient age         : "  + rs.getString(3));
//				System.out.println("Patient gender      : "  + rs.getString(4));
//				System.out.println("Patient contact No. : "  + rs.getString(5));
//				System.out.println("Patient diseases    : "  + rs.getString(6));
//				System.out.println("Patient status      : "  + rs.getString(7));
			}		
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	
	public static boolean updatePateint(String query) {
		
//		if (!chcekExistance(patientID))
//			return;
//		
//		System.out.println("what do you want to update in patient : ");
//		
//		System.out.println("Enter 1 for name     :");
//		System.out.println("Enter 2 for age      :");
//		System.out.println("Enter 4 for contact  :");
//		System.out.println("Enter 3 for gender   :");
//		System.out.println("Enter 5 for disease  :");
//		System.out.println("Enter 6 for status   :");
//	
//		Scanner scan = new Scanner(System.in);
//		int choice = Integer.parseInt(scan.nextLine());
//		
//		String query = null;
//		String placeholder = null;
//		
//		switch(choice) {
//		
//		case (1):
//			System.out.println("Enter the new name of patient :");
//			placeholder = scan.nextLine();
//			query = "update patient set Name = \"" + placeholder + "\" where Patient_ID = " + patientID +"";
//			break;
//		
//		case (2):
//			System.out.println("Enter the new Age of Patient :");
//			placeholder = scan.nextLine();
//			query = "update patient set Age = \"" + placeholder + "\" where Patient_ID = " + patientID +"";
//			break;
//			
//		case (3):
//			System.out.println("Enter the new Contact No. of Patient :");
//			placeholder = scan.nextLine();
//			query = "update patient set Contact_No = \"" + placeholder + "\" where Patient_ID = " + patientID +"";
//			break;
//		
//		case (4):
//			System.out.println("Enter the new Gender of Patient :");
//			placeholder = scan.nextLine();
//			query = "update patient set Gender = \"" + placeholder + "\" where Patient_ID = " + patientID +"";
//			break;
//		
//		case (5):
//			System.out.println("Enter the new disease of Patient :");	
//			placeholder = scan.nextLine();
//			query = "update patient set Disease = \"" + placeholder + "\" where Patient_ID = " + patientID +"";	
//			break;
//			
//		case (6):
//			System.out.println("Enter the new status of Patient :");	
//			placeholder = scan.nextLine();
//			query = "update patient set Status = \"" + placeholder + "\" where Patient_ID = " + patientID +"";	
//			break;
//			
//		default :
//			
//			System.out.println(PageFormate.wrongChoice);
//			break;
//		
//		}
		
		try(java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
			
			statement.executeUpdate(query);
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return false;
	}
	
}
