package com.divergentsl.cms.dao;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.divergentsl.cms.utils.ConnectionUtil;
import com.divergentsl.cms.utils.PageFormate;

public class AppointmentOperation {

	public static boolean createAppointment(int doctorId, int patientId, String time, String date) {
		// TODO Auto-generated method stub
		
		try(java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
			
			String query = "insert into Appointment(Doctor_ID,Patient_ID,Time,Date) values ("+ doctorId +"," + patientId + ",\"" + time + "\",\"" + date + "\");";
			statement.execute(query);
			
			return true;
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}

	
	
	public static List<String> getAppointment(int doctorId) {
		
		if (!DoctorOperations.checkExistance(doctorId)) {
			System.out.println(PageFormate.pageSeprater);
			return null;
		}
		
		List<String> appointments = new ArrayList<>();
		String query = "select DISTINCT(Patient_ID) from appointment where Doctor_ID = " + doctorId + " order by Appointment_ID DESC;";
		
		try(java.sql.Connection connection = ConnectionUtil.createConncetion(); Statement statement = connection.createStatement()){
			
			ResultSet rs = statement.executeQuery(query);
			
			while(rs.next()) {
				appointments.add(rs.getString(1));
			}
			
			return appointments;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
