package com.divergentsl.cms.patients;

import java.util.Scanner;

import com.divergentsl.cms.dao.InvoiceOperations;
import com.divergentsl.cms.dao.PatientOperation;
import com.divergentsl.cms.dao.PrescriptionOperation;
import com.divergentsl.cms.dto.Invocie;
import com.divergentsl.cms.dto.Prescription;
import com.divergentsl.cms.utils.PageFormate;

import com.divergentsl.cms.login.Loign;

public class PatientHomePage {

	public static void authenticatePatient() {
		
		System.out.println(PageFormate.pageSeprater);
		System.out.println("\n---------------------------------- PATIENT AUTHENTICATE PAGE -------------------------------\n");
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter 0 for returning to home page : ");
		System.out.println("Enter the patient ID : ");
		
		int patientId = 0;
		
		try {
			patientId = Integer.parseInt(scan.nextLine());
		}catch(NumberFormatException e) {
			System.out.println(PageFormate.invalidInput);
			authenticatePatient();
		}
		
		if(patientId == 0) {
			Loign.showLoginMenu();
			return;
		}
			
		if(!PatientOperation.chcekExistance(patientId)) {
			System.out.println("Wrong Patient ID try again");
			System.out.println(PageFormate.pageSeprater);
			return;
		}

		welcomePatient(patientId);
		
	}
	public static void welcomePatient(int patientId) {
			
			System.out.println(PageFormate.pageSeprater);
			System.out.println("---------------------------------- Welcome Patient to patient home page -------------------------------");
			Scanner scan = new Scanner(System.in);
			
			while(true) {
				
			System.out.println("\n---------------------------------- Patient Home Page -------------------------------\n");
			
			try {
			
			System.out.println("\n------> Enter your choice : ");
			
			System.out.println("Enter 1 for view prescription : ");
			System.out.println("Enter 2 for view invoice :");
			System.out.println("Enter 3 for going back login module :");
			
			int choice = Integer.parseInt(scan.nextLine());
			
			switch(choice) {
			
			case (1):
				System.out.println(PageFormate.pageSeprater);
				Prescription.getPrescritionInfo(patientId);
				System.out.println(PageFormate.pageSeprater);
				break;
				
			case (2):
				System.out.println(PageFormate.pageSeprater);
				Invocie.getInvoiceInfo(patientId);
				System.out.println(PageFormate.pageSeprater);
				break;
			
			case (3):
				Loign.showLoginMenu();
				break;
				
			default:
				System.out.println(PageFormate.wrongChoice);
				break;
			}
			
			}catch(NumberFormatException e) {
				System.out.println(PageFormate.invalidInput);
				System.out.println(PageFormate.pageSeprater);
			}
			
		}
		}
}
